#
FROM php:7.3.33-fpm-alpine

ENV COMPOSER_ALLOW_SUPERUSER 1 \
    TZ ${TZ:-"Europe/Moscow"}

RUN apk --no-cache add tzdata mc zip wget bash harfbuzz-utils

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

ADD https://raw.githubusercontent.com/mlocati/docker-php-extension-installer/master/install-php-extensions /usr/local/bin/

RUN chmod uga+x /usr/local/bin/install-php-extensions && sync

RUN install-php-extensions gd intl zip mysqli pdo_mysql xdebug calendar exif imagick memcached pcntl sockets tidy wddx xsl uuid gettext soap

RUN set -xe \
    && apk add --no-cache --update --virtual .phpize-deps $PHPIZE_DEPS \
    && pecl install -o -f redis  \
    && echo "extension=redis.so" > /usr/local/etc/php/conf.d/redis.ini \
    && rm -rf /usr/share/php \
    && rm -rf /tmp/* \
    && apk del  .phpize-deps

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

COPY conf.d/php-extend.ini /usr/local/etc/php/conf.d/php-extend.ini


EXPOSE 9000
CMD ["php-fpm"]
